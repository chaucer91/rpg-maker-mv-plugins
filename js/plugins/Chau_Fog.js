/*:============================================================================

  @author Chaucer

  @plugindesc | GL Fog : Version - 1.0.1 | Add randomly generated fog to your game.

  @help
============================================================================
  Introduction :
============================================================================

  ()()
  (^.^)
  c(")(")

  Adds fog to your game by using a filter, no images required.

============================================================================
  Requirements :
============================================================================

  ---------------------------------------
  None.
  ---------------------------------------

============================================================================
  Instructions :
============================================================================

  ---------------------------------------
  Plugin Commands :
  ---------------------------------------

   command : gl_fog color R G B speed S scrollX X scrollY Y octaves O density D
---------------------------------------
   description : Allows you to modify the gl_fog values.
---------------------------------------
   example : gl_fog color 191 127 178 speed 0.2 scrollX 0 scrollY 0 octaves 3
   example : gl_fog color 191. 102 89 speed 0.1 octaves 5 fadein 10
   example : gl_fog color 89 102 0 speed 0.1 scrollX 0.5 octaves 2
   example : gl_fog color 140 204 140
   example : gl_fog density 1.5
   example : gl_fog fadeout 5
   example : gl_fog fadein 5
   example : gl_fog clear

   argument[gl_fog] : color R G B
---------------------------------------
   description : Define the color of the fog R( red ) G( green ) and B( blue )
   are whole number values, between 0 ( no color ) and 255 ( full color ),
   each value should be seperated by a space.alternatively, script snippets,
   can be spliced in instead of a number.
---------------------------------------
   example : color 255 100 50
   example : color $gameVariable.value(1) 100 $gameVariable.value(2)

   argument[gl_fog] ( optional ) : speed S
---------------------------------------
   description : Replace S with the speed at which the fog animates, S is a
   float value, a number between 0 ( no animation ) and 1 ( fast animation ).
   If the parameter is not included, it will default to 0.
---------------------------------------
   example : speed 0.02

   argument[gl_fog] ( optional ) : scrollX X
---------------------------------------
   description : X represents the speed at which the fog scrolls on the X axis.
   If the parameter is not included, it will default to 0.
---------------------------------------
   example : scrollX 1.0

   argument[gl_fog] ( optional ) : scrollY Y
---------------------------------------
   description : Y represents the speed at which the fog scrolls on the Y axis.
   If the parameter is not included, it will default to 0.
---------------------------------------
   example : scrollY 1.0

   argument[gl_fog] ( optional ) : octaves O
---------------------------------------
   description : octaves controls how smooth or scattered the fog appears,
   try adjusting the value between a number of 1 ~ 10( i recommend staying
   above 2 ), the lower the number, the smoother the fog, the higher the number
   the more ruffled the fog will be. If the parameter is not included, it will
   default to 4.
---------------------------------------
   example : scrollX 1.0

   argument[gl_fog] ( optional ) : fadein T
---------------------------------------
   description : adding fadein will tell the it should fade in at T seconds.
   if T is not specified, T will be defaulted to 1 second.
---------------------------------------
   example : fadein 5

   argument[gl_fog] ( optional ) : fadeout T
---------------------------------------
   description : adding fadeout will tell the it should fade out at T seconds.
   if T is not specified, T will be defaulted to 1 second.
---------------------------------------
   example : fadeout 5

   argument[gl_fog] ( optional ) : density D
---------------------------------------
   description : how thick the fog will appear on screen, default value is 1.
---------------------------------------
   example : fadeout 5

   argument[gl_fog] ( optional ) : clear
---------------------------------------
   description : clear the fog filter.
---------------------------------------
   example : clear


  ---------------------------------------
  Note Tags : [Actors][Enemies]
  ---------------------------------------

     command :
     <fog_barrier>
     x X
     y Y
     scaleX SX
     scaleY SY
     maxX MX
     maxY MY
     type T
     </fog_barrier>
---------------------------------------
     description : setup a fog barrier with the arguments specified, see below
     for a detailed list of what each arguments purpose is.
---------------------------------------
     example:
     <fog_barrier>
     x 0
     y -10
     scaleX 1
     scaleY 1.5
     maxX 3.0
     maxY 3.5
     </fog_barrier>

     argument[fog_barrier] ( optional ): x X
---------------------------------------
    description : set the offset X position of the fog barrier, replace X with
    the offset of the barrier( in pixels ).

     argument[fog_barrier] ( optional ): y Y
---------------------------------------
    description : set the offset Y position of the fog barrier, replace X with
    the offset of the barrier( in pixels ).

     argument[fog_barrier] ( optional ) : scaleX SX
---------------------------------------
    description : Affects the size of the fog barrier, SX is a multiplier,
    if you set it's value to 2, the barier will be twice as wide as it is by
    default.

     argument[fog_barrier] ( optional ) : scaleX SY
---------------------------------------
    description : Affects the size of the fog barrier, SY is a multiplier,
    if you set it's value to 2, the barier will be twice as tall as it is by
    default.

     argument[fog_barrier] ( optional ) : maxX MX
---------------------------------------
    description : Determines the max size the barrier can grow to( on the
    X axis )while the character is walking.

     argument[fog_barrier] ( optional ) : maxX MY
---------------------------------------
    description : Determines the max size the barrier can grow to( on the
    Y axis )while the character is walking.

  ---------------------------------------
  Script calls :
  ---------------------------------------

    N/A
============================================================================
  Terms Of Use :
============================================================================

  This Plugin may be used commercially, or non commercially. This plugin may
  be extended upon, and or shared freely as long as credit is given to it's
  author(s). This plugin may NOT be sold, or plagiarized.

============================================================================
  Version History :
============================================================================

  ● Version : 1.0.0
  ● Date : 11/16/2019
    ★ Release.

  ● Version : 1.0.1
  ● Date : 01/31/2020
    ✩ updated documentation.

============================================================================
  Contact Me :
============================================================================

  If you have questions, about this plugin, or commissioning me, or have
  a bug to report, please feel free to contact me by any of the below
  methods.

  rmw : https://forums.rpgmakerweb.com/index.php?members/chaucer.44456
  discord : chaucer#7538
  skypeId : chaucer1991
  gmail : chaucer91

============================================================================
  Support Me :
============================================================================

   If you like the content I create, and want to contribute to help me
  making more plugins on a regular basis, you can donate, or pledge through
  any of the links listed below!

  ko-fi : https://ko-fi.com/chaucer91
  paypal.me : https://paypal.me/chaucer91
  patreon : https://www.patreon.com/chaucer91

============================================================================

    @param playerBarrier
    @text Player Fog Barrier
    @desc The barrier that will be used around the player.
    @default {"ox":"0","oy":"0","sx":"1.25","sy":"1.25","mx":"1.75","my":"1.75"}
    @type struct<FogBarrier>

    @param followerBarrier
    @text Follower Fog Barrier
    @desc The barrier that will be used around the player.
    @default {"ox":"0","oy":"0","sx":"1.25","sy":"1.25","mx":"1.25","my":"1.25"}
    @type struct<FogBarrier>
    @parent playerBarrier

    @param vehicleBarrier
    @text Vehicle Fog Barrier
    @desc The barrier that will be used around the player.
    @default {"ox":"0","oy":"0","sx":"1.25","sy":"1.25","mx":"2.00","my":"2.00"}
    @type struct<FogBarrier>

*/

 /*~struct~FogBarrier:

  @param ox
  @text X
  @desc The offset x of the fog barrier around the character.
  @default 0
  @type number

  @param oy
  @text Y
  @desc The offset y of the fog barrier around the character.
  @default 0
  @type number

  @param sx
  @text scaleX
  @desc The scale of the fog barrier on the x axis.
  @default 1.00
  @type number
  @decimals 2

  @param sy
  @text scaleY
  @desc The scale of the fog barrier on the y axis.
  @default 1.00
  @type number
  @decimals 2

  @param mx
  @text maxX
  @desc The max size of the fog barrier on the x axis when character is moving.
  @default 1.00
  @type number
  @decimals 2

  @param my
  @text maxY
  @desc The max size of the fog barrier on the y axis when character is moving.
  @default 1.00
  @type number
  @decimals 2

*/

//=============================================================================
var Imported = Imported || {};
Imported['CHAU FOG'] = true;
//=============================================================================
var Chaucer = Chaucer || {};
Chaucer.fog = {};
//=============================================================================

//=============================================================================
// Fog_Filter :
//=============================================================================

//=============================================================================

class Fog_Filter extends PIXI.Filter
{ // Fog_Filter

//=============================================================================
  constructor( color, speed, scroll )
  { // Called on object creation.
//=============================================================================

    super(
      Chaucer.fog.vertexShader(), Chaucer.fog.fragmentShader(),
      Chaucer.fog.uniforms( color )
    );

    this.createCharacters();
    this.scrollSpeed = $gameTemp.fogScroll() || scroll || { x: 0.0, y: 0.0 };
    this.speed = $gameTemp.fogSpeed() || speed || 0.0;
    this._initBarriers = true;
    this.updateBarriers();
    this._initBarriers = false;
  }

//=============================================================================
// DEFINE PROPERTIES HERE :
//=============================================================================

  set scroll ( value ) { this.uniforms.scroll = value; };
  get scroll () { return this.uniforms.scroll; };

  set value ( value ) { this.uniforms.value = value; };
  get value () { return this.uniforms.value; };

  set alpha ( value ) { this.uniforms.alpha = value; };
  get alpha () { return this.uniforms.alpha; };

  set shake ( value ) { this.uniforms.shake = value; };
  get shake () { return this.uniforms.shake; };

  set zoomX ( value ) { this.uniforms.zoom[0] = value; };
  get zoomX () { return this.uniforms.zoom[0]; };

  set zoomY ( value ) { this.uniforms.zoom[1] = value; };
  get zoomY () { return this.uniforms.zoom[1]; };

  set color ( value ) { this.uniforms.color = value; };
  get color () { return this.uniforms.color; };

  set density ( value ) { this.uniforms.density = value; };
  get density () { return this.uniforms.density; };

  set zoomScale ( value ) { this.uniforms.zoom[2] = value; };
  get zoomScale () { return this.uniforms.zoom[2]; };

//=============================================================================

//=============================================================================
  fadeIn( speed = 0.05 )
  { // fade out the filter.
//=============================================================================

    this._fadeStatus = 1;
    this._fadeSpeed = speed;

  }


//=============================================================================
  fadeOut( speed = 0.05 )
  { // fade out the filter.
//=============================================================================

    this._fadeStatus = -1;
    this._fadeSpeed = speed;

  }

//=============================================================================
  createCharacters()
  { // create all character data.
//=============================================================================

    this.characters = Chaucer.fog.getCharacters();

  }

//=============================================================================
  apply( filterManager, input, output )
  { // write a new apply method.
//=============================================================================

    if ( this.uniforms.dimensions ) {

      this.uniforms.dimensions[0] = input.sourceFrame.width;
      this.uniforms.dimensions[1] = input.sourceFrame.height;
    }

    filterManager﻿.applyFilter( this, input, output );

  }

//=============================================================================
  updateShader()
  { // update the time of the shader.
//=============================================================================

    if ( !!this._fadeStatus ) this.updateFade();

    if ( this.alpha > 0.0 ) {
      this.updateZoomShake();
      this.updateBarriers();
      this.updateScroll();

    }

  }

//=============================================================================
  updateFade()
  { // update the fade value of the filter.
//=============================================================================

    var speed = this._fadeSpeed * this._fadeStatus;
    this.alpha = ( this.alpha + speed ).clamp( 0.0, 1.0 );

    if ( this.alpha === 0.0 || this.alpha === 1.0 )
      this._fadeStatus = this._fadeSpeed = 0;

  }

//=============================================================================
  updateBarriers()
  { // update the character fog barrier data.
//=============================================================================

    var isMap = SceneManager._scene instanceof Scene_Map;
    var isBattle = SceneManager._scene instanceof Scene_Battle;

    for ( var i = 0, l = this.characters.length; i < l; i++ ) {

      if ( isMap ) {
        this.setMapBarriers( i, this.characters[i] );
        this.setMapBarrierPositions( i, this.characters[i] );

      } else if ( isBattle ) {
        this.setBattleBarriers( i,this.characters[i] );
        this.setBattleBarrierPositions( i, this.characters[i] );
      }

    }

  }

//=============================================================================
  isCharacterVisible( character )
  { // Return if this character is visible.
//=============================================================================

    if ( SceneManager._scene instanceof Scene_Map ) {
      var isFollower = character.constructor === Game_Follower
      if ( SceneManager.isNextScene( Scene_Battle ) ) return false;
      if ( isFollower && !character.isVisible() ) return false;
      if ( character.isTransparent() ) return false;
      if ( character._erased ) return false;

    } else if ( SceneManager._scene instanceof Scene_Battle ) {
      if ( !character._battler.isAppeared() ) return false;

    }

    return true;

  }

//=============================================================================
  setMapBarriers( index, character )
  { // set the alpha for map character barriers.
//=============================================================================

    var barrier = character._fogBarrier;
    var r = character.isMoving() ? 0.05 : -0.05;
    var sx = $gameScreen.zoomScale() * barrier.sx;
    var sy = $gameScreen.zoomScale() * barrier.sy;
    var width = this.uniforms['barrier' + index][2];
    var height = this.uniforms['barrier' + index][3];
    var minX = barrier.sx * sx, maxX = barrier.mx * sx;
    var minY = barrier.sy * sy, maxY = barrier.my * sy;
    var visible = this.isCharacterVisible( character );

    width = visible ? this.getBarrierWidth( width, minX, maxX, r ) : 0.0;
    height = visible ? this.getBarrierHeight( height, minY, maxY, r ) : 0.0;

    this.uniforms['barrier' + index][2] = width;
    this.uniforms['barrier' + index][3] = height;

  }

//=============================================================================
  getBarrierWidth( width, min, max, r )
  { // set the barrier width.
//=============================================================================

    if ( width < min && !this._initBarriers )
      width = ( width + Math.abs( r ) ).clamp( width, max );

    else if ( width > max && !this._initBarriers )
      width = ( width + -Math.abs( r ) ).clamp( min, width );

    else
      width = ( width + r ).clamp( min, max );

    return width;

  }

//=============================================================================
  getBarrierHeight( height, min, max, r )
  { // set the barrier height.
//=============================================================================

    if( height < min && !this._initBarriers )
      height = ( height + Math.abs( r ) ).clamp( height, max );

    else if ( height > max && !this._initBarriers )
      height = ( height + -Math.abs( r ) ).clamp( min, height );

    else
      height = ( height + r ).clamp( min, max );

    return height;

  }
//=============================================================================
  setMapBarrierPositions( i, c )
  { // set the position of the characters fog barrier.
//=============================================================================

    var zX = $gameScreen.zoomX(), zY = $gameScreen.zoomY();
    var gw = Graphics.width, gh = Graphics.height;
    var cx = c.screenX(), cy = c.screenY() - 24;
    var ox = c._fogBarrier.ox, oy = c._fogBarrier.oy;
    var s = $gameScreen.zoomScale() - 1;

    this.uniforms['barrier' + i][0] = ( cx + ( cx - zX ) * s + ox ) / gw;
    this.uniforms['barrier' + i][1] = ( cy + ( cy - zY ) * s + oy ) / gh;

  }

//=============================================================================
  setBattleBarriers( index, character )
  { // set the alpha for battler barriers.
//=============================================================================

    var barrier = character._battler._fogBarrier;
    var sx = $gameScreen.zoomScale() * barrier.sx;
    var sy = $gameScreen.zoomScale() * barrier.sy;
    var width = this.uniforms['barrier' + index][2];
    var height = this.uniforms['barrier' + index][3];
    var r = character._movementDuration ? 0.05 : -0.05;
    var minX = barrier.sx * sx, maxX = barrier.mx * sx;
    var minY = barrier.sy * sy, maxY = barrier.my * sy;
    var visible = this.isCharacterVisible( character );

    width = visible ? ( width + r ).clamp( minX, maxX ) : 0.0;
    height = visible ? ( height + r ).clamp( minY, maxY ) : 0.0;

    this.uniforms['barrier' + index][2] = width;
    this.uniforms['barrier' + index][3] = height;

  }

//=============================================================================
  setBattleBarrierPositions( i, b )
  { // set the position of the battlers fog barriers.
//=============================================================================

    var ox = b._battler._fogBarrier.ox, oy = b._battler._fogBarrier.oy;
    var zX = $gameScreen.zoomX(), zY = $gameScreen.zoomY();
    var gw = Graphics.width, gh = Graphics.height;
    var s = $gameScreen.zoomScale()  - 1;
    var cx = b._homeX + b._offsetX;
    var cy = b._homeY + b._offsetY;

    this.uniforms['barrier' + i][0] = ( cx + ( cx * zX ) * s + ox )  / gw;
    this.uniforms['barrier' + i][1] = ( cy + ( cy * zY ) * s + oy )  / gh;

  }

//=============================================================================
  updateZoomShake()
  { // update zoom and shake of the fog.
//=============================================================================

    var gw = Graphics.width;
    var gh = Graphics.height;

    this.shake = $gameScreen.shake() / gw;
    this.zoomScale = $gameScreen.zoomScale();
    this.zoomX = ( $gameScreen.zoomX() * ( this.zoomScale - 1 ) ) / gw;
    this.zoomY = ( $gameScreen.zoomY() * ( this.zoomScale - 1 ) ) / gh;

  }

//=============================================================================
  updateScroll()
  { // update scroll of the fog..
//=============================================================================

      this.value = ( this.value  + this.speed / 1000.0 ) % 100.0;
      this.scroll[0] += this.scrollSpeed.x;
      this.scroll[1] += this.scrollSpeed.y;

  }

};

//=============================================================================
// Spriteset_Base
//=============================================================================

//-----------------------------------------------------------------------------
Spriteset_Base.prototype.createFogFilter = function ()
{ // create fog filter.
//-----------------------------------------------------------------------------

  Chaucer.fog._fogSpriteset = this;
  var color = { r: 0.0, g: 0.0, b: 0.0 };
  this._fog = new Fog_Filter( color, 0.0, { x: 0.0, y: 0.0 } );
  this.filters = ( this.filters || [] ).concat( [this._fog] );

};

//-----------------------------------------------------------------------------
Spriteset_Base.prototype.removeFogFilter = function ()
{ // remove the current fog filter.
//-----------------------------------------------------------------------------

  var filters = [];

  for (var i = 0, l = this.filters.length; i < l; i++) {
    if ( this.filters[i] === this._fog ) continue;
    filters.push( this.filters[i] );
  }

  this._fog = null;
  this.filters = filters;

};

//=============================================================================
// Scene_Base :
//=============================================================================

//-----------------------------------------------------------------------------
Scene_Base.prototype.createFogFilter = function ()
{ // create a new fogFilter.
//-----------------------------------------------------------------------------

  if ( this._spriteset ) return this._spriteset.createFogFilter();

};

//-----------------------------------------------------------------------------
Scene_Base.prototype.removeFogFilter = function ()
{ // create a new fogFilter.
//-----------------------------------------------------------------------------

  if ( this._spriteset ) return this._spriteset.removeFogFilter();

};

//-----------------------------------------------------------------------------
Scene_Base.prototype.updateFog = function ()
{ // update the fog.
//-----------------------------------------------------------------------------

  var fog = this.fogFilter();
  if ( fog ) fog.updateShader();

};

//-----------------------------------------------------------------------------
Scene_Base.prototype.fogFilter = function ()
{ // return the current fog filter of the scene.
//-----------------------------------------------------------------------------

  if ( this._spriteset ) return this._spriteset._fog;
  return null;

};

//=============================================================================
// Game_Interpreter :
//=============================================================================

//-----------------------------------------------------------------------------
Game_Interpreter.prototype.setupFog = function ( fog, args )
{ // set fog based on args.
//-----------------------------------------------------------------------------

    for ( var i = 0, l = args.length; i < l; i++ ) {
      var arg = args[i].toLowerCase();
      if ( arg === 'clear' ) { // CLEAR FOG:
        this.clearFog( fog );

      } else if ( arg === 'color' ) { // SET COLOR:
        this.setFogColor( fog, args[i + 1], args[i + 2], args[i + 3] );

      } else if ( arg === 'density' ) { // SET COLOR:
        fog.density = eval( args[i + 1] );

      } else if ( arg === 'speed' ) { // SET SPEED:
        fog.speed = eval( args[i + 1] );

      } else if ( arg === 'fadein' ) { // START FADEIN:
        fog.alpha = 0.0;
        fog.fadeIn( ( ( 1 / 60 ) / eval( args[i + 1] ) || 1.0 ) );

      } else if ( arg === 'fadeout' ) { // START FADEOUT:
        fog.alpha = 1.0;
        fog.fadeOut( ( ( 1 / 60 ) / eval( args[i + 1] ) || 1.0 ) );

      } else if ( arg === 'scrollx' ) { // SET SCROLL SPEED X:
        fog.scrollSpeed.x = eval( args[i + 1] ) / 1500.0;

      } else if ( arg === 'scrolly' ) { // SET SCROLL SPEED Y:
        fog.scrollSpeed.y = eval( args[i + 1] ) / 1500.0;

      }
    }

    fog.updateShader();

};

//-----------------------------------------------------------------------------
Game_Interpreter.prototype.refreshFogFilter = function ( args )
{ // remove the fog filter and append a new one.
//-----------------------------------------------------------------------------

  for (var i = 0, l = args.length; i < l; i++) {

    if ( args[i] === 'octaves' ) Chaucer.fog.octaves = args[i + 1];

  }
  if ( Chaucer.fog.octaves !== undefined ) {
    SceneManager._scene.removeFogFilter();
    SceneManager._scene.createFogFilter();
    Chaucer.fog.octaves = undefined;
  }

};

//-----------------------------------------------------------------------------
Game_Interpreter.prototype.clearFog = function ( fog )
{ // clear the fog filter.
//-----------------------------------------------------------------------------

  fog.color[0] = 0;
  fog.color[1] = 0;
  fog.color[2] = 0;

};

//-----------------------------------------------------------------------------
Game_Interpreter.prototype.setFogColor = function ( fog, r, g, b )
{ // set the fog color.
//-----------------------------------------------------------------------------

  fog.color[0] = ( eval( r ) || 0.0 ) / 255;
  fog.color[1] = ( eval( g ) || 0.0 ) / 255;
  fog.color[2] = ( eval( b ) || 0.0 ) / 255;

};

//=============================================================================
// Game_Temp :
//=============================================================================

//-----------------------------------------------------------------------------
Game_Temp.prototype.saveFog = function ( fog )
{ // save the current fog.
//-----------------------------------------------------------------------------

  if ( fog ) {

    this._savedFog = {
      scrollSpeed: fog.scrollSpeed,
      fragmentSrc: fog.fragmentSrc,
      vertexSrc: fog.vertexSrc,
      uniforms: fog.uniforms,
      speed: fog.speed,
    };

  }

};

//-----------------------------------------------------------------------------
Game_Temp.prototype.fogScroll = function ()
{ // Return the scroll speed of the saved fog.
//-----------------------------------------------------------------------------

  if ( !this._savedFog ) return 0.0;
  return this._savedFog.scrollSpeed;

};

//-----------------------------------------------------------------------------
Game_Temp.prototype.fogUniforms = function ()
{ // return the vertex source for the saved fog.
//-----------------------------------------------------------------------------

  if ( !this._savedFog ) return null;
  var uniforms = this._savedFog.uniforms;

  return uniforms;

};

//-----------------------------------------------------------------------------
Game_Temp.prototype.fogFragmentSrc = function ()
{ // return the vertex source for the saved fog.
//-----------------------------------------------------------------------------

  if ( !this._savedFog ) return null;
  if ( SceneManager.isNextScene( Scene_Battle ) ) {

    var { uBarrier, applyBarrier } = Chaucer.fog.getBarrierData();
    fragmentSrc = this._savedFog.fragmentSrc.split( '\n' );
    var last = -1;

    for ( var i = 0, l = fragmentSrc.length; i < l; i++ ) {
      if ( fragmentSrc[i] && fragmentSrc[i].match( /barrier\d+/ ) ) {

        if ( last !== i ) {
          fragmentSrc[i] = last === -1 ? '\t<a>' : '\t\t<b>';
          last = i + 1;

        } else {
          fragmentSrc.splice( i--, 1 );

        }

      }
    }

    return fragmentSrc.join( '\n' )
      .replace( '<a>', uBarrier ).replace( '<b>', applyBarrier );

  }

  return this._savedFog.fragmentSrc;

};

//-----------------------------------------------------------------------------
Game_Temp.prototype.fogVertexSrc = function ()
{ // return the vertex source for the saved fog.
//-----------------------------------------------------------------------------

  if ( !this._savedFog ) return null;
  return this._savedFog.vertexSrc;

};

//-----------------------------------------------------------------------------
Game_Temp.prototype.fogSpeed = function ()
{ // return the speed of the fog animation.
//-----------------------------------------------------------------------------

  if ( !this._savedFog ) return 0.0;
  return this._savedFog.speed;

};

//-----------------------------------------------------------------------------
Game_Temp.prototype.clearFog = function ()
{ // clear the currently saved fog.
//-----------------------------------------------------------------------------

  this._savedFog = null;

};

//=============================================================================
// Game_CharacterBase :
//=============================================================================

//-----------------------------------------------------------------------------
Game_CharacterBase.prototype.initFogBarrier = function ()
{ // initialize fog barrier for the character.
//-----------------------------------------------------------------------------

  this._fogBarrier = { ox: 0, oy: 0, sx: 1, sy: 1, mx: 0, my: 0 };

};

//-----------------------------------------------------------------------------
Game_CharacterBase.prototype.fogBarrier = function ()
{ // return if this has a fog barrier.
//-----------------------------------------------------------------------------

  return !!this._fogBarrier.alpha;

};

//-----------------------------------------------------------------------------
Game_CharacterBase.prototype.applyFogBarrierArgs = function ( args )
{ // apply fog barrier arguments.
//-----------------------------------------------------------------------------

  for ( var i = 0; i < args.length; i += 2 ) {

    switch ( args[i].toLowerCase() ) {
      case 'x':
        this.setFogBarrierOx( eval( args[i + 1] ) );
        break;
      case 'y':
        this.setFogBarrierOy( eval( args[i + 1] ) );
        break;
      case 'scalex':
        this.setFogBarrierScaleX( eval( args[i + 1] ) );
        break;
      case 'scaley':
        this.setFogBarrierScaleY( eval( args[i + 1] ) );
        break;
      case 'maxx':
        this.setFogBarrierMaxX( eval( args[i + 1] ) );
        break;
      case 'maxy':
        this.setFogBarrierMaxY( eval( args[i + 1] ) );
        break;
    }

  }

};

//-----------------------------------------------------------------------------
Game_CharacterBase.prototype.setFogBarrierOx = function ( offsetX )
{ // set the fog barriers offset x.
//-----------------------------------------------------------------------------

  this._fogBarrier.ox = offsetX;

};

//-----------------------------------------------------------------------------
Game_CharacterBase.prototype.setFogBarrierOy = function ( offsetY )
{ // set the fog barriers offset y.
//-----------------------------------------------------------------------------

  this._fogBarrier.oy = offsetY;

};

//-----------------------------------------------------------------------------
Game_CharacterBase.prototype.setFogBarrierScaleX = function ( scaleX )
{ // set the fog barriers scale x.
//-----------------------------------------------------------------------------

  this._fogBarrier.sx = scaleX;

};

//-----------------------------------------------------------------------------
Game_CharacterBase.prototype.setFogBarrierScaleY = function ( scaleY )
{ // set the fog barriers scale y.
//-----------------------------------------------------------------------------

  this._fogBarrier.sy = scaleY;

};

//-----------------------------------------------------------------------------
Game_CharacterBase.prototype.setFogBarrierMaxX = function ( maxX )
{ // set the fog barriers max x.
//-----------------------------------------------------------------------------

  this._fogBarrier.mx = maxX;

};

//-----------------------------------------------------------------------------
Game_CharacterBase.prototype.setFogBarrierMaxY = function ( maxY )
{ // set the fog barriers max y.
//-----------------------------------------------------------------------------

  this._fogBarrier.my = maxY;

};

//=============================================================================
// Game_Event :
//=============================================================================

//-----------------------------------------------------------------------------
Game_Event.prototype.setupPageFogBarrier = function ()
{ // setup the fog barrier for the current event page.
//-----------------------------------------------------------------------------

  var args = [];
  var fetching = false;
  var list = this.page().list;

  for (var i = 0, l = list.length; i < l; i++) {
    var { code, parameters }= list[i];

    if ( code !== 108 && code !== 408 ) continue;
    if ( fetching && parameters[0] === '</fog_barrier>' ) fetching = false;
    if ( fetching ) args = args.concat( parameters[0].split( ' ' ) );
    if ( parameters[0] === '<fog_barrier>' ) fetching = true;
  }

  this.applyFogBarrierArgs( args );

};

//=============================================================================
// Game_BattlerBase :
//=============================================================================

//-----------------------------------------------------------------------------
Game_BattlerBase.prototype.initFogBarrier = function ()
{ // initialize fog barrier values.
//-----------------------------------------------------------------------------

  this._fogBarrier = { ox: 0, oy: 0, sx: 1, sy: 1, mx: 0, my: 0 };

};

//-----------------------------------------------------------------------------
Game_BattlerBase.prototype.setFogBarrierOx = function ( offsetX )
{ // set the fog barriers offset x.
//-----------------------------------------------------------------------------

  this._fogBarrier.ox = offsetX;

};

//-----------------------------------------------------------------------------
Game_BattlerBase.prototype.setFogBarrierOy = function ( offsetY )
{ // set the fog barriers offset y.
//-----------------------------------------------------------------------------

  this._fogBarrier.oy = offsetY;

};

//-----------------------------------------------------------------------------
Game_BattlerBase.prototype.setFogBarrierScaleX = function ( scaleX )
{ // set the fog barriers scale x.
//-----------------------------------------------------------------------------

  this._fogBarrier.sx = scaleX;

};

//-----------------------------------------------------------------------------
Game_BattlerBase.prototype.setFogBarrierScaleY = function ( scaleY )
{ // set the fog barriers scale y.
//-----------------------------------------------------------------------------

  this._fogBarrier.sy = scaleY;

};

//-----------------------------------------------------------------------------
Game_BattlerBase.prototype.setFogBarrierMaxX = function ( maxX )
{ // set the fog barriers max x.
//-----------------------------------------------------------------------------

  this._fogBarrier.mx = maxX;

};

//-----------------------------------------------------------------------------
Game_BattlerBase.prototype.setFogBarrierMaxY = function ( maxY )
{ // set the fog barriers max y.
//-----------------------------------------------------------------------------

  this._fogBarrier.my = maxY;

};

//-----------------------------------------------------------------------------
Game_BattlerBase.prototype.applyFogBarrierNote = function ( notes )
{ // apply fog barrier based on notes.
//-----------------------------------------------------------------------------

  var args = [];
  var fetching = false;
  var lines = notes.split( '\n' );

  for ( var i = 0, l = lines.length; i < l; i++ ) {
    if ( lines[i] === '</fog_barrier>' ) fething = false;
    if ( fetching ) args = args.concat( lines[i].split( ' ' ) )
    if ( lines[i] === '<fog_barrier>' ) fetching = true;
  }

  this.applyFogBarrierArgs( args );

};

//-----------------------------------------------------------------------------
Game_BattlerBase.prototype.applyFogBarrierArgs = function ( args )
{ // apply fog barrier arguments.
//-----------------------------------------------------------------------------

  for ( var i = 0; i < args.length; i += 2 ) {

    switch ( args[i].toLowerCase() ) {
      case 'x':
        this.setFogBarrierOx( eval( args[i + 1] ) );
        break;
      case 'y':
        this.setFogBarrierOy( eval( args[i + 1] ) );
        break;
      case 'scalex':
        this.setFogBarrierScaleX( eval( args[i + 1] ) );
        break;
      case 'scaley':
        this.setFogBarrierScaleY( eval( args[i + 1] ) );
        break;
      case 'maxx':
        this.setFogBarrierMaxX( eval( args[i + 1] ) );
        break;
      case 'maxy':
        this.setFogBarrierMaxY( eval( args[i + 1] ) );
        break;
    }

  }

};

//=============================================================================
// Game_Actor :
//=============================================================================

//=============================================================================
// Game_Enemy :
//=============================================================================

( function ( $ ) { // CONFIG:

  $ = $ || {};

//Create plugin information.
//=============================================================================

  $.errMessage = [
    "GL Fog description has been altered! Either revert",
    "the description of the plugin back to normal, or modify the variable",
    "named pluginIdentifier inside the plugin to prevent further errors!!!"
  ].join( '\n' );

var pluginIdentifier = /(Fog) : Version - (\d+\.\d+\.\d+)/;
   var length = $plugins.length;
  for ( var i = 0; i < length; i++ ) {
    if ( $plugins[i].description.match( pluginIdentifier ) ) {
      $.alias = {};
      $.name = RegExp.$1;
      $.version = RegExp.$2;
      $.params = Parse( $plugins[i].parameters );
      $.author = "Chaucer";
      break;
    };
  };

  if ( !$.name || !$.version ) throw new Error( $.errMessage );

//=============================================================================

//=============================================================================
// Custom :
//=============================================================================

  //--------------------------------------------------------------------------
  function Parse( object )
  { // parse all data in an object
  //--------------------------------------------------------------------------

    try {
      object = JSON.parse( object );
     } catch (e) {
      object = object;
     } finally {
      if ( Array.isArray( object ) ) {
        var l = object.length;
        for ( var i = 0; i < l; i++ ) { object[i] = Parse( object[i] ); };
      } else if ( typeof object === 'object' ) {
        for ( var key in object ) { object[key] = Parse( object[key] ); };
      }
     }
     return object;

  };

  //-----------------------------------------------------------------------------
  $.versionLessThan = function ( version )
  { // check if the version is less than the value specified.
  //-----------------------------------------------------------------------------

    var comparison = version.split( '.' );
    var current = $.version.split( '.' );
    if ( current[0] < comparison[0] ) return true;
    if ( current[1] < comparison[1] ) return true;
    if ( current[2] < comparison[2] ) return true;

  };

  //-----------------------------------------------------------------------------
  $.versionGreaterThan = function ( version )
  { // check if the version is greater than or equal to the value specified.
  //-----------------------------------------------------------------------------

    var comparison = version.split( '.' );
    var current = $.version.split( '.' );
    if ( current[0] >= comparison[0] ) return true;
    if ( current[1] >= comparison[1] ) return true;
    if ( current[2] >= comparison[2] ) return true;

  };

//-----------------------------------------------------------------------------
// create barrier object
//-----------------------------------------------------------------------------
  $.barrierRadius = 256;
  $.barrierHRadius = $.barrierRadius / 2;
  $.barrier = new PIXI.Graphics();
  $.barrier.beginFill( 0x000000, 1.0 );
  $.barrier.drawRect( 0, 0, $.barrierRadius, $.barrierRadius );

  for ( var i = 0; i < $.barrierHRadius; i++ ) {

    var r = ( $.barrierHRadius / 2 ) + $.barrierHRadius - i;

    $.barrier.beginFill( 0x000000, 1 );
    $.barrier.drawCircle( $.barrierHRadius, $.barrierHRadius, r );

    $.barrier.beginFill( 0xffffff, ( i / $.barrierHRadius ) );
    $.barrier.drawCircle( $.barrierHRadius, $.barrierHRadius, r );

  }

//-----------------------------------------------------------------------------
  $.getCharacters = function() {
//-----------------------------------------------------------------------------

    if ( SceneManager._scene instanceof Scene_Battle ) {
      var spriteset = this._fogSpriteset;
      return spriteset._actorSprites.concat( spriteset._enemySprites );
    } else {
      return [$gamePlayer].concat(
        $gamePlayer._followers._data ).concat( $gameMap.events() )
        .concat( $gameMap.vehicles() );
    }

  }

//-----------------------------------------------------------------------------
  $.getBarrierData = function() {
//-----------------------------------------------------------------------------

    var uBarrier = ``;
    var applyBarrier = ``;
    var characters = this.getCharacters();

    for (var i = 0, l = characters.length; i < l; i++) {
      uBarrier += 'uniform vec4 barrier' + i + ';\n\t';
      applyBarrier += `fog *= drawBarrier( ${'barrier' + i} );\n\t`
    }

    return { uBarrier: uBarrier, applyBarrier: applyBarrier };

  }

//-----------------------------------------------------------------------------
  $.uniforms = function( color = { r:0.0, g: 0.0, b: 0.0 } ) {
//-----------------------------------------------------------------------------

    var barrierTexture = Graphics._renderer.generateTexture( $.barrier );

    var uniforms = {
      dimensions: { type: 'v2', value: [Graphics.width, Graphics.height] },
      color: { type: 'v3', value: [color.r, color.g, color.b] },
      barrier: { type: 'sampler2D', value: barrierTexture },
      zoom: { type: 'v4', value: [0.0, 0.0, 1.0, 1.0] },
      scroll: { type: 'v2', value: [0.0, 0.0] },
      density: { type: 'f', value: 0.0 },
      value: { type: 'f', value: 0.0 },
      alpha: { type: 'f', value: 1.0 },
      shake: { type: 'f', value: 1.0 },
    };

    this.addCharacterUniforms( uniforms );
    this.applySavedUniforms( uniforms );
    return uniforms;

  };

//-----------------------------------------------------------------------------
  $.addCharacterUniforms = function( uniforms ) {
//-----------------------------------------------------------------------------

    var characters = this.getCharacters();
    var gw = Graphics.width, gh = Graphics.height;
    var isMap = SceneManager._scene instanceof Scene_Map;

    for ( var i = 0, l = characters.length; i < l; i++ ) {

      var c = characters[i];
      var x = ( isMap ? c._realX : ( c._homeX + c._offsetX ) )  / gw;
      var y = ( isMap ? c._realY : ( c._homeY + c._offsetY ) )  / gh;
      uniforms['barrier' + i] = { type: 'v4', value: [x, y, 1.0, 1.0] };

    }

  }

//-----------------------------------------------------------------------------
  $.applySavedUniforms = function ( uniforms )
  { // Apply any saved uniform values to the fog filters uniforms.
//-----------------------------------------------------------------------------

    var savedUniforms = $gameTemp.fogUniforms();

    if ( savedUniforms ) {

      var isBattle = SceneManager._scene instanceof Scene_Battle;
      var keys = Object.keys( savedUniforms );

      for ( var i = 0, l = keys.length; i < l; i++ ) {
        var isBarrier = !uniforms[keys[i]] || keys[i].includes( 'barrier' );
        if ( isBattle && isBarrier ) continue;
        uniforms[keys[i]].value = savedUniforms[keys[i]];
      }

    }

  };

//-----------------------------------------------------------------------------
  $.vertexShader = function() {
//-----------------------------------------------------------------------------

    if ( $gameTemp.fogVertexSrc() ) return $gameTemp.fogVertexSrc();

    return `
    precision mediump float;

    attribute vec2 aVertexPosition;
    attribute vec2 aTextureCoord;

    uniform mat3 projectionMatrix;
    uniform mat3 filterMatrix;
    uniform vec2 dimensions;
    uniform vec4 filterArea;
    uniform vec3 zoom;

    varying vec2 vTextureCoord;
    varying vec2 fTextureCoord;
    varying vec2 vFilterCoord;

    void main ( void ) {

      vec3 projected = projectionMatrix * vec3( aVertexPosition, 1 );
      gl_Position = vec4( ( projected ).xy, 0.0, 1.0 );
      vFilterCoord = ( filterMatrix * vec3( aTextureCoord, 1.0) ).xy;
      vTextureCoord = aTextureCoord;

      vec2 z = ( zoom.xy * filterArea.xy ) / dimensions;
      fTextureCoord = ( aTextureCoord + z / 2.0 ) * ( 1.0 / zoom.z );

    }
    `;

  }

//-----------------------------------------------------------------------------
  $.fragmentShader = function() { // return a new fragment shader.
//-----------------------------------------------------------------------------

    if ( $gameTemp.fogFragmentSrc() ) return $gameTemp.fogFragmentSrc();
    var { uBarrier, applyBarrier } = this.getBarrierData();

    return `
    precision mediump float;
    precision mediump int;

    varying vec2 vTextureCoord;
    varying vec2 fTextureCoord;

    uniform sampler2D uSampler;
    uniform sampler2D barrier;
    uniform vec2 dimensions;
    uniform vec4 filterArea;
    uniform float density;
    uniform float value;
    uniform float alpha;
    uniform float shake;
    uniform vec2 scroll;
    uniform vec3 color;
    uniform vec3 zoom;
    ${uBarrier}

    float rand ( vec2 coord ) {

      float r1 = 15.0 + ${Math.random()} * 60.0;
      float r2 = 15.0 + ${Math.random()} * 60.0;
      float r3 = 4000.0 + ${Math.random()} * 1000.0;
      return fract( sin( dot( coord, vec2( r1, r2 ) ) * 1000.0 ) * r3 );

    }


    float noise( vec2 coord ) {

      vec2 i = floor( coord );
      vec2 f = fract( coord );

      float a = rand( i );
      float b = rand( i + vec2( 1.0, 0.0 ) );
      float c = rand( i + vec2( 0.0, 1.0 ) );
      float d = rand( i + vec2( 1.0, 1.0 ) );

      vec2 u = f * f * ( 3.0 - 2.0 * f );

      return ( mix( a, b, u.x )  + ( c -a ) * u.y * ( 1.0 - u.x ) + ( d - b ) * u.x * u.y );

    }

    float fbm ( vec2 coord ) {

      float value = 0.0;
      float amplitude = 0.5;
      float frequency = 0.8;
      const int octaves = ${ $.octaves || 5 };

      for ( int i = 0; i < octaves; i++ ) {
        value += amplitude * noise( coord * ( 2.25 ) );
        coord *= 2.0;
        amplitude *= 0.5;
      }

      return value;

    }

    float drawBarrier ( vec4 p ) {

      float w = 48.0 * p.z;
      float h = 48.0 * p.w;
      float hw = w / 2.0;
      float hh = h / 2.0;
      vec2 ofst = vec2( ( hw ) / dimensions[0], ( hh ) / dimensions[1] );
      vec2 scale = vec2( w / dimensions[0], h / dimensions[1] );
      vec2 coord = ( ( vTextureCoord * filterArea.xy ) / dimensions );
      coord = ( ( p.xy + ofst + vec2( shake, 0.0 ) ) - coord ) / ( scale );
      vec4 lSampler = texture2D( barrier, coord ) * 0.95;

      return 1.0 - lSampler.r;

    }

    void main () {

      vec4 fog = vec4( 0.0, 0.0, 0.0, 0.0 );

      if ( alpha > 0.0 ) {
        float twoPi = 6.283185307179586;
        float v = sin( mod( value, 100.0 ) * twoPi / 100.0 ) * 200.0;
        vec2 coord = ( fTextureCoord * filterArea.xy ) / dimensions;
        float colorMax = color.r + color.g + color.b;
        coord = coord + scroll;
        coord.x -= shake;

        vec2 q = vec2(
          fbm( coord + 0.0 * v ), fbm( coord + vec2( 1.0 ) )
        );

        vec2 r = vec2(
          fbm( coord + 1.0 * q + vec2( 2.7,7.2 ) + 0.15 * v ),
          fbm( coord + 1.0 * q + vec2( 5.3,2.8 ) + 0.126 * v )
        );

        float f = fbm( coord / 1.0 + r );
        vec3 fColor = color * alpha;
        fog = vec4( ( f * f * f + 0.3 * f * f + 0.25 * f ) * ( fColor ), 0.0 );
        fog.rgb *= 1.0 + density;
        ${applyBarrier}

      }

      gl_FragColor = texture2D( uSampler, vTextureCoord ) + fog;

    }
    `;

  }

//=============================================================================
// Spriteset_Base :
//=============================================================================

//-----------------------------------------------------------------------------
  $.alias.Ss_B_p_initialize = Spriteset_Base.prototype.initialize;
//-----------------------------------------------------------------------------
  Spriteset_Base.prototype.initialize = function ()
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    $.alias.Ss_B_p_initialize.call( this );
    this.createFogFilter();

  };

//-----------------------------------------------------------------------------
  $.alias.SB_p_update = Scene_Base.prototype.update;
//-----------------------------------------------------------------------------
  Scene_Base.prototype.update = function ()
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    $.alias.SB_p_update.call( this );
    this.updateFog();

  };

//=============================================================================
// Scene_Map :
//=============================================================================

//-----------------------------------------------------------------------------
  $.alias.SM_p_termiante = Scene_Map.prototype.terminate;
//-----------------------------------------------------------------------------
  Scene_Map.prototype.terminate = function ()
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    if ( !SceneManager.isNextScene( Scene_Map ) )
      $gameTemp.saveFog( this.fogFilter() );

    $.alias.SM_p_termiante.call( this );

  };

//=============================================================================
// Scene_Battle :
//=============================================================================

//-----------------------------------------------------------------------------
  $.alias.Class_method = Scene_Battle.prototype.terminate;
//-----------------------------------------------------------------------------
  Scene_Battle.prototype.terminate = function ()
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------
    if ( $gameTemp._savedFog ) {
      $gameTemp.saveFog( this.fogFilter() );
    }
    $.alias.Class_method.call( this );

  };


//=============================================================================
// Game_Interpreter :
//=============================================================================

//-----------------------------------------------------------------------------
  $.alias.GI_p_pluginCommand = Game_Interpreter.prototype.pluginCommand;
//-----------------------------------------------------------------------------
  Game_Interpreter.prototype.pluginCommand = function ( command, args )
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    $.alias.GI_p_pluginCommand.call( this, command, args );
    if ( command.toLowerCase() === 'gl_fog' ) {
      this.refreshFogFilter( args );
      this.setupFog( SceneManager._scene.fogFilter(), args );
    }

  };


//=============================================================================
// Game_Map :
//=============================================================================

//-----------------------------------------------------------------------------
  $.alias.GM_p_scrollDown = Game_Map.prototype.scrollDown;
//-----------------------------------------------------------------------------
  Game_Map.prototype.scrollDown = function ( scrollDistance )
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    var lastY = this._displayY;
    var th = $gameMap.tileHeight();
    var filter = SceneManager._scene.fogFilter();

    $.alias.GM_p_scrollDown.call( this, scrollDistance );

    if ( this.isLoopVertical() ) {

      filter.scroll[1] += ( scrollDistance * th ) / Graphics.height;

    } else if ( this.height() >= this.screenTileY() ) {

      scrollDistance = this._displayY - lastY;
      filter.scroll[1] += ( scrollDistance * th ) / Graphics.height;
    }

  };

//-----------------------------------------------------------------------------
  $.alias.GM_p_scrollUp = Game_Map.prototype.scrollUp;
//-----------------------------------------------------------------------------
  Game_Map.prototype.scrollUp = function ( scrollDistance )
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    var lastY = this._displayY;
    var th = $gameMap.tileHeight();
    var filter = SceneManager._scene.fogFilter();

    $.alias.GM_p_scrollUp.call( this, scrollDistance );

    if ( this.isLoopVertical() ) {

      filter.scroll[1] -= ( scrollDistance * th ) / Graphics.height;

    } else if ( this.height() >= this.screenTileY() ) {

      scrollDistance = lastY - this._displayY;
      filter.scroll[1] -= ( scrollDistance * th ) / Graphics.height;
    }

  };

//-----------------------------------------------------------------------------
  $.alias.GM_p_scrollLeft = Game_Map.prototype.scrollLeft;
//-----------------------------------------------------------------------------
  Game_Map.prototype.scrollLeft = function ( scrollDistance )
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    var lastX = this._displayX;
    var tw = $gameMap.tileWidth();
    var filter = SceneManager._scene.fogFilter();

    $.alias.GM_p_scrollLeft.call( this, scrollDistance );

    if ( this.isLoopHorizontal() ) {

      filter.scroll[0] -= ( scrollDistance * tw ) / Graphics.width;

    } else if ( this.width() >= this.screenTileX() ) {

      scrollDistance = lastX - this._displayX;
      filter.scroll[0] -= ( scrollDistance * tw ) / Graphics.width;

    }

  };

//-----------------------------------------------------------------------------
  $.alias.GM_p_scrollRight = Game_Map.prototype.scrollRight;
//-----------------------------------------------------------------------------
  Game_Map.prototype.scrollRight = function ( scrollDistance )
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    var lastX = this._displayX;
    var tw = $gameMap.tileWidth();
    var filter = SceneManager._scene.fogFilter();

    $.alias.GM_p_scrollRight.call( this, scrollDistance );

    if ( this.isLoopHorizontal() ) {

    filter.scroll[0] += ( scrollDistance * tw ) / Graphics.width;

  } else if ( this.width() >= this.screenTileX() ) {

    scrollDistance = this._displayX - lastX;
    filter.scroll[0] += ( scrollDistance * tw ) / Graphics.width;

  }

  };

//=============================================================================
// Game_Temp :
//=============================================================================

//-----------------------------------------------------------------------------
  $.alias.GT_p_initialize = Game_Temp.prototype.initialize;
//-----------------------------------------------------------------------------
  Game_Temp.prototype.initialize = function ()
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    $.alias.GT_p_initialize.call( this );
    this._savedFog = null;

  };

//=============================================================================
// Game_CharacterBase :
//=============================================================================

//-----------------------------------------------------------------------------
  $.alias.GCB_p_initMembers = Game_CharacterBase.prototype.initMembers;
//-----------------------------------------------------------------------------
  Game_CharacterBase.prototype.initMembers = function ()
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    $.alias.GCB_p_initMembers.call( this );
    this.initFogBarrier();

  };

//=============================================================================
// Game_Player :
//=============================================================================

//-----------------------------------------------------------------------------
  $.alias.GP_p_initMembers = Game_Player.prototype.initMembers;
//-----------------------------------------------------------------------------
  Game_Player.prototype.initMembers = function ()
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    $.alias.GP_p_initMembers.call( this );
    this._fogBarrier = Object.assign( {}, $.params.playerBarrier );

  };

//=============================================================================
// Game_Follower :
//=============================================================================

//-----------------------------------------------------------------------------
  $.alias.GF_p_initialize = Game_Follower.prototype.initialize;
//-----------------------------------------------------------------------------
  Game_Follower.prototype.initialize = function ( memberIndex )
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    $.alias.GF_p_initialize.call( this, memberIndex );
    this._fogBarrier = Object.assign( {}, $.params.followerBarrier );

  };

//=============================================================================
// Game_Vehicle :
//=============================================================================

//-----------------------------------------------------------------------------
  $.alias.GV_p_initMembers = Game_Vehicle.prototype.initMembers;
//-----------------------------------------------------------------------------
  Game_Vehicle.prototype.initMembers = function ()
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    $.alias.GV_p_initMembers.call( this );
    this._fogBarrier = Object.assign( {}, $.params.vehicleBarrier );

  };

//=============================================================================
// Game_Event :
//=============================================================================

//-----------------------------------------------------------------------------
  $.alias.GE_p_setupPageSettings = Game_Event.prototype.setupPageSettings;
//-----------------------------------------------------------------------------
  Game_Event.prototype.setupPageSettings = function ()
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    $.alias.GE_p_setupPageSettings.call( this );
    this.setupPageFogBarrier();

  };

//=============================================================================
// Game_BattlerBase :
//=============================================================================

//-----------------------------------------------------------------------------
  $.alias.G_BB_p_initMembers = Game_BattlerBase.prototype.initMembers;
//-----------------------------------------------------------------------------
  Game_BattlerBase.prototype.initMembers = function ()
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    $.alias.G_BB_p_initMembers.call( this );
    this.initFogBarrier();

  };

//=============================================================================
// Game_Actor :
//=============================================================================

//-----------------------------------------------------------------------------
  $.alias.GA_p_setup = Game_Actor.prototype.setup;
//-----------------------------------------------------------------------------
  Game_Actor.prototype.setup = function ( actorId )
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    $.alias.GA_p_setup.call( this, actorId );
    this.applyFogBarrierNote( this.actor().note );

  };

//=============================================================================
// Game_Enemy :
//=============================================================================

//-----------------------------------------------------------------------------
  $.alias.GE_p_setup = Game_Enemy.prototype.setup;
//-----------------------------------------------------------------------------
  Game_Enemy.prototype.setup = function ( enemyId, x, y )
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    $.alias.GE_p_setup.call( this, enemyId, x, y );
    this.applyFogBarrierNote( this.enemy().note );

  };

//-----------------------------------------------------------------------------
  $.alias.GE_p_transform = Game_Enemy.prototype.transform;
//-----------------------------------------------------------------------------
  Game_Enemy.prototype.transform = function ( enemyId )
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    $.alias.GE_p_transform.call( this, enemyId );
    this.applyFogBarrierNote( this.enemy().note );

  };

//=============================================================================
} )( Chaucer.fog );
//=============================================================================
