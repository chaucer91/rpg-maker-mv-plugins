/*:===========================================================================//

  @author Chaucer

  @plugindesc | Transition Effects : Version - 1.0.0 | This plugin allows you to use transition effects like in previous versions of rpg maker.

  @help
===============================================================================
  Introduction :
===============================================================================

  ()()
  (^.^)
  c(")(")

  Bring back transition effects from rpg maker xp/vx with some extra features!

===============================================================================
  Requirements :
===============================================================================

  ---------------------------------------
  None.
  ---------------------------------------

===============================================================================
  Instructions :
===============================================================================

  ---------------------------------------
  Plugin Commands :
  ---------------------------------------

   command : gl_transition ARGUMENT
  ---------------------------------------
  description : start a gl transition effect with the spettings specified.
  ---------------------------------------
  example :
  gl_transition transition_effect target picture 10
  file img/transitions/perlin-height-map speed 0.02
  color red 1 blue 0 green 0 alpha 1 threshold 10
  ---------------------------------------
  example:
  transition_effect target event 7 file img/transitions/010-Random02
  speed 0.005 threshold 5 color red 0 blue 0.8 green 0.5 alpha 0
  fadein

    argument[gl_transition] : target T ID
---------------------------------------
    description : specify the target for the transition effect. replace T with
    either "screen", "event", "follower", "player", "actor", "enemy", or
    "picture".
---------------------------------------
    example : gl_transition transition_effect target picture 10
    example : gl_transition transition_effect target screen
    example : gl_transition transition_effect target player

    argument[gl_transition] : file F
---------------------------------------
    description : specify the noise mask file for the transition. replace F
    with the path to the image file.
    ---------------------------------------
    example : gl_transition file img/transitions/005-Stripe01
    example : gl_transition file img/transitions/006-Stripe02

    argument[gl_transition] : speed S
---------------------------------------
    description : Replace S with speed at which you want the transition to
    execute at.
    ---------------------------------------
    example : gl_transition speed 0.005

    argument[gl_transition] ( optional ): color red R green G blue B  alpha A
---------------------------------------
    description : Set the color of of the transitions fade, numbers are in
    float values( number between 0, and 1, 0 being no color, 1 is full color ),
    R =  red value, G = blue value, B = blue value, A = alpha value( alpha is
    the level of transparency, 0 being fully transparent, 1 being solid color ).
---------------------------------------
    example : gl_transition color 1.0, 0.0, 0.0
    example : gl_transition color 0.0, 1.0, 1.0

    argument[gl_transition] ( optional ) : threshold TH
---------------------------------------
    description : Set the threshold for the color, the lower the threshold, the
    tighter the fade area will be, the lower the number the more prominent the
    gradient will be.
---------------------------------------
    example : gl_transition threshold 10

    argument[gl_transition] ( optional ) : fadein
---------------------------------------
    description : the transition effect will play in reverse, and the image
    will transition in, instead of transitioning out.
---------------------------------------
    example : gl_transition fadein

    argument[gl_transition] ( optional ) : wait
---------------------------------------
    description : if this argument is added, the event will not continue until
    the transition effect is finished!
---------------------------------------
    example : gl_transition wait

  ---------------------------------------
  Script calls :
  ---------------------------------------

===============================================================================
  Terms Of Use :
===============================================================================

  This Plugin may be used commercially, or non commercially. This plugin may
  be extended upon, and or shared freely as long as credit is given to it's
  author(s). This plugin may NOT be sold, or plagiarized.

===============================================================================
  Version History :
===============================================================================

  ● Version : 1.0.0
  ● Date : 01/31/2020
    ★ Release.

===============================================================================
  Contact Me :
===============================================================================

  If you have questions, about this plugin, or commissioning me, or have
  a bug to report, please feel free to contact me by any of the below
  methods.

  rmw : https://forums.rpgmakerweb.com/index.php?members/chaucer.44456
  discord : chaucer#7538
  skypeId : chaucer1991
  gmail : chaucer91

===============================================================================
  Support Me :
===============================================================================

   If you like the content I create, and want to contribute to help me
  making more plugins on a regular basis, you can donate, or pledge through
  any of the links listed below!

  ko-fi : https://ko-fi.com/chaucer91
  paypal.me : https://paypal.me/chaucer91
  patreon : https://www.patreon.com/chaucer91

===============================================================================

  @param enableBTransition
  @text GL Battle Transition
  @desc Enable gl battle transition with the below settings specified.
  @default false
  @type boolean

  @param noiseMask
  @text Noise Mask
  @desc The image that will be used for transitions by default.
  @default
  @type file
  @dir img/
  @require 1
  @parent enableBTransition

  @param speed
  @text Transition Speed
  @desc how fast the transition plays.
  @default 0.500
  @type number
  @decimals 3
  @min 0.001
  @max 1.0
  @parent enableBTransition

  @param threshold
  @text Threshold
  @desc lower numbers will produce softer edges, while higher, will result in harder edges.
  @default 1.0
  @type number
  @decimals 1
  @max 50.0
  @min 1.0
  @parent enableBTransition

  @param color
  @text Glow Color
  @desc The color of the glow on edges.
  @default {"r":"0.00","g":"0.00","b":"0.00","a":"0.00"}
  @type struct<Color>
  @parent enableBTransition

*/

 /*~struct~Color:

  @param r
  @text Red
  @desc Red Value( Number between 0, and 1, 0= no value, 1=full value ).
  @default 1.00
  @type number
  @decimals 3
  @min 0.0
  @max 1.0

  @param g
  @text Green
  @desc Green Value( Number between 0, and 1, 0= no value, 1=full value ).
  @default 1.00
  @type number
  @decimals 3
  @min 0.0
  @max 1.0

  @param b
  @text Blue
  @desc Blue Value( Number between 0, and 1, 0= no value, 1=full value ).
  @default 1.00
  @type number
  @decimals 3
  @min 0.0
  @max 1.0

  @param a
  @text Alpha
  @desc The strength of the color specified( Number between 0, and 1, 0= no value, 1=full value ).
  @default 0.00
  @type number
  @decimals 3
  @min 0.0
  @max 1.0

*/

//=============================================================================
var Imported = Imported || {};
Imported['TRANSITION EFFECTS'] = true;
//=============================================================================
var Chaucer = Chaucer || {};
Chaucer.transitions = {};
//=============================================================================


//=============================================================================
// Transition_Filter :
//=============================================================================

//=============================================================================
class Transition_Filter extends PIXI.Filter
{ // Transition_Filter

//=============================================================================
  constructor( bitmap, color, threshold )
  { // Called on object creation.
//=============================================================================

    super(
      Chaucer.transitions.vertexShader,
      Chaucer.transitions.fragmentShader,
      Chaucer.transitions.uniforms( bitmap, color, threshold )
    );

    this.setSpeed( Chaucer.transitions.params.speed );
    this.addTicker();
    this.padding = 0;

  };


//=============================================================================
// DEFINE VARIABLES HERE :
//=============================================================================
  get value() { return this.uniforms.value; };
  set value( value ) { this.uniforms.value = value; };
//=============================================================================


//=============================================================================
  start()
  { // tell the sprite to start updating.
//=============================================================================

    this._ticker.start();

  }

//=============================================================================
  stop()
  { // stop the ticker from updating.
//=============================================================================

    this._ticker.stop();

  }
//=============================================================================
  addTicker()
  { // Add a ticker to handle self updating.
//=============================================================================

    this._ticker = new PIXI.ticker.Ticker();
    this._ticker.add( this.update.bind( this ) );

  }

//=============================================================================
  apply( filterManager, input, output )
  { // write a new apply method.
//=============================================================================

    if ( this.uniforms.dimensions ) {
      this.uniforms.dimensions[0] = input.sourceFrame.width
      this.uniforms.dimensions[1] = input.sourceFrame.height
    }

    filterManager﻿.applyFilter( this, input, output );

  }

//=============================================================================
  setSpeed( value )
  { // set the speed of this transition effect.
//=============================================================================

    this._speed = value;

  }
//=============================================================================
  reset()
  { // reset the filter to defualt value.
    //=============================================================================

    this.uniforms.value = 1.0;

  };


//=============================================================================
  fadeIn()
  { // tell the filter to fade in.
//=============================================================================

    if ( !this.isBusy() ) {
      this._fadeStatus = 'in';
      this.start();
    }

  }

//=============================================================================
  fadeOut()
  { // tell the filter to fade out.
    //=============================================================================

    if ( !this.isBusy() ) {
      this._fadeStatus = 'out';
      this.start();
    }

  }

//=============================================================================
  isBusy()
  { // return if we're fading in any direction.
//=============================================================================

    return !!this._fadeStatus;

  }

//=============================================================================
  update()
  { // update the transition filter.
//=============================================================================

    if ( this._fadeStatus === 'in' ) this.updateFadeIn();
    if ( this._fadeStatus === 'out' ) this.updateFadeOut();
    if ( !this.isBusy() ) this.stop();

  }

//=============================================================================
  updateFadeIn()
  { // update fade in.
//=============================================================================

    this.value = ( this.value + this._speed ).clamp( 0.0, 1.0 );
    if ( this.value === 1.0 ) this._fadeStatus = '';

  }

//=============================================================================
  updateFadeOut()
  { // update fade in.
//=============================================================================

    this.value = ( this.value - this._speed ).clamp( 0.0, 1.0 );
    if ( this.value === 0.0 ) this._fadeStatus = '';

  }

};

//=============================================================================
// Scene_Map :
//=============================================================================

//-----------------------------------------------------------------------------
Scene_Map.prototype.createBattleTransition = function ()
{ // create the transition filter.
//-----------------------------------------------------------------------------

  this._bTransitionFilter = new Transition_Filter();
  this.filters = ( this.filters || [] ).concat( [this._bTransitionFilter] );

};

//-----------------------------------------------------------------------------
Scene_Map.prototype.createTransitionFilter = function ( bitmap, color, threshold, speed )
{ // create the transition filter.
//-----------------------------------------------------------------------------

  if ( this._transitionFilter ) this.removeTransitionFilter();
  this._transitionFilter = new Transition_Filter( bitmap, color, threshold );
  this.filters = ( this.filters || [] ).concat( [this._transitionFilter] );
  this._transitionFilter.setSpeed( speed );

};

//-----------------------------------------------------------------------------
Scene_Map.prototype.removeTransitionFilter = function ()
{ // remove the current transition filter..
//-----------------------------------------------------------------------------

  if ( this.filters.length > 1 ) {
    var filters = [];
    for (var i = 0, length = this.filters.length; i < length; i++) {
      if ( this.filters[i] === this._transitionFilter ) continue;
      filters.push( this.filters[i] );
    }

  } else {
    this.filters = null;
  }

  this._transitionFilter = null;

};

//-----------------------------------------------------------------------------
Scene_Map.prototype.transitionOut = function ()
{ // transition out the sprite.
//-----------------------------------------------------------------------------

  if ( this._transitionFilter ) this._transitionFilter.fadeOut();

};

//-----------------------------------------------------------------------------
Scene_Map.prototype.transitionIn = function ()
{ // transition out the sprite.
//-----------------------------------------------------------------------------

  if ( this._transitionFilter ) this._transitionFilter.fadeIn();

};


//=============================================================================
// Sprite_Picture :
//=============================================================================

//-----------------------------------------------------------------------------
Sprite.prototype.createTransitionFilter = function ( bitmap, color, threshold, speed )
{ // apply a new transition filter to a sprite.
//-----------------------------------------------------------------------------

  if ( this._transitionFilter ) this.removeTransitionFilter();
  this._transitionFilter = new Transition_Filter( bitmap, color, threshold );
  this.filters = ( this.filters || [] ).concat( [this._transitionFilter] );
  this._transitionFilter.setSpeed( speed );

};

//-----------------------------------------------------------------------------
Sprite.prototype.removeTransitionFilter = function ()
{ // remove the current transition filter..
//-----------------------------------------------------------------------------

  if ( this.filters.length > 1 ) {
    var filters = [];
    for (var i = 0, length = this.filters.length; i < length; i++) {
      if ( this.filters[i] === this._transitionFilter ) continue;
      filters.push( this.filters[i] );
    }
  } else {
    this.filters = null;
  }

  this._transitionFilter = null;

};

//-----------------------------------------------------------------------------
Sprite.prototype.transitionOut = function ()
{ // transition out the sprite.
//-----------------------------------------------------------------------------

  if ( this._transitionFilter ) this._transitionFilter.fadeOut();

};

//-----------------------------------------------------------------------------
Sprite.prototype.transitionIn = function ()
{ // transition out the sprite.
  //-----------------------------------------------------------------------------

  if ( this._transitionFilter ) this._transitionFilter.fadeIn();

};

//=============================================================================
// Game_Interpreter :
//=============================================================================

//-----------------------------------------------------------------------------
Game_Interpreter.prototype.requestTransitionEffect = function ( args )
{ // Descriptions.
//-----------------------------------------------------------------------------

  var options = this.makeTransitionOptions( args );

  switch ( options.target.type ) {
    case 'screen':
      target = this.transitionScreenTarget( options.target );
      break;
    case 'event':
    case 'player':
    case 'follower':
      target = this.transitionCharacterTarget( options.target );
      break;
    case 'actor':
    case 'enemy':
      target = this.transitionBattlerTarget( options.target );
      break;
    case 'picture':
      target = this.transitionPictureTarget( options.target );
      break;
  }

  if ( target ) this.startTransitionEffect( target, options );

}

//-----------------------------------------------------------------------------
Game_Interpreter.prototype.startTransitionEffect = function ( target, options )
{ // start transition effect on target.
//-----------------------------------------------------------------------------
  console.log( options );
  target.createTransitionFilter(
    options.bitmap, options.color, options.threshold, options.speed
  );

  if ( options.fadeStatus === 'fadein' ) {
    target._transitionFilter.value = 0.0;
    target.transitionIn();

  } else {
    target.transitionOut();
  }

  this._transition = target._transitionFilter;
  if ( options.wait ) this._waitMode = 'transition';

};

//-----------------------------------------------------------------------------
Game_Interpreter.prototype.setTransitionFile = function ( index, args, options )
{ // make the transition target for the options object.
  //-----------------------------------------------------------------------------

  var splitIndex = args[index + 1].lastIndexOf( '/' ) + 1;
  var folder = args[index + 1].slice( 0, splitIndex );
  var filename = args[index + 1].slice( splitIndex, args[index + 1].length );

  if ( filename && folder ) {
    options.bitmap = ImageManager.loadBitmap( folder, filename );

  } else {
    options.bitmap = $.bitmap;
  }

};

//-----------------------------------------------------------------------------
Game_Interpreter.prototype.setTransitionTarget = function ( index, args, options )
{ // make the transition target for the options object.
//-----------------------------------------------------------------------------

  options.target = { type: args[1], index: -1 };

  if ( args[index + 1] === 'actor' || args[index + 1] === 'enemy' ) {
    options.target.index = Number( args[index + 2] );

  } else if ( args[index + 1] === 'picture' ) {
    options.target.index = Number( args[index + 2] );

  } else if ( args[index + 1] === 'event' ) {
    options.target.index = Number( args[index + 2] );

  } else if ( args[index + 1] === 'follower' ) {
    options.target.index = Number( args[index + 2] );
  }

};

//-----------------------------------------------------------------------------
Game_Interpreter.prototype.setTransitionColor = function ( index, args, options )
{ // make the transition target for the options object.
//-----------------------------------------------------------------------------

  options.color = { r: 0.0, g: 0.0, b: 0.0, a: 0.0 };

  for ( var i = 0; i < 4; i++ ) {
    var color = args[index + 1 + i * 2];
    var value = args[index + 2 + i * 2];
    options.color[color[0]] = Number( value );
  }

};

//-----------------------------------------------------------------------------
Game_Interpreter.prototype.makeTransitionOptions = function ( args )
{ // Organize/setup argument values.
//-----------------------------------------------------------------------------
  var color = [];
  var command = '';
  var options = { fadeStatus: 'out' };
  var colors = [ 'red','green', 'blue', 'alpha' ];
  var commands = [
    'wait', 'color', 'threshold', 'file', 'speed', 'target', 'fadein'
  ];

  for (var i = 0, length = args.length; i < length; i++) {

    if ( commands.includes( args[i] ) ) {
      command = args[i].toLowerCase();
      switch ( command ) {
        case 'file':
          this.setTransitionFile( i, args, options );
          break;
        case 'target':
          this.setTransitionTarget( i, args, options );
          break;
        case 'color':
          this.setTransitionColor( i, args, options );
          break;
        case 'fadein':
          options.fadeStatus = 'fadein';
          break;
        case 'wait':
          options.wait = true;
          break;
        default:
          options[command] = Number( args[i + 1] );
      }

    }

  }
  return options;
};

//-----------------------------------------------------------------------------
Game_Interpreter.prototype.transitionScreenTarget = function ( options )
{ // start screen transition.
//-----------------------------------------------------------------------------

  return SceneManager._scene;

};

//-----------------------------------------------------------------------------
Game_Interpreter.prototype.transitionCharacterTarget = function ( options )
{ //return the proper map character target.
//-----------------------------------------------------------------------------

  var gameObj = null;
  var characterSprites = SceneManager._scene._spriteset._characterSprites;

  switch ( options.type ) {
    case 'event':
      gameObj = $gameMap.event( options.index );
      break;
    case 'player':
      gameObj = $gamePlayer;
      break;
    case 'follower':
      gameObj = $gamePlayer.followers()._data[options.index - 1];
      break;
  }

  for ( var i = 0, length = characterSprites.length; i < length; i++ ) {
    if ( gameObj === characterSprites[i]._character ) {
      return characterSprites[i];
    }
  }

  return null;

};

//-----------------------------------------------------------------------------
Game_Interpreter.prototype.transitionBattlerTarget = function ( options )
{ // start transition for battler.
//-----------------------------------------------------------------------------

  var spriteset = SceneManager._scene._spriteset;
  var sprites = [];

  switch ( options.type ) {
    case 'actor':
      sprites = spriteset._actorSprites;
      break;
    case 'enemy':
      if ( $gameSystem.isSideView() ) sprites = spriteset._enemySprites;
      break;
  }

  for ( var i = 0, length = sprites.length; i < length; i++ ) {
    var battler = sprites[i]._battler;
    if ( !battler ) continue;
    if ( battler.isActor() && battler.actorId() === options.index ) {
      return sprites[i];

    } else if ( battler.index() === options.index - 1 ) {
      return sprites[i];
    }
  }

  return null;

};

//-----------------------------------------------------------------------------
Game_Interpreter.prototype.transitionPictureTarget = function ( options )
{ // start transition for battler.
//-----------------------------------------------------------------------------

  var pictures = SceneManager._scene._spriteset._pictureContainer.children;

  for ( var i = 0, length = pictures.length; i < length; i++ ) {
    if ( pictures[i]._pictureId === options.index ) return pictures[i];
  }

  return null;

};

( function ( $ ) { // CONFIG:

  $ = $ || {};

//Create plugin information.
//============================================================================

  const errMessage = [
    "Transition Effects description has been altered! Either revert",
    "the description of the plugin back to normal, or modify the variable",
    "named pluginIdentifier inside the plugin to prevent further errors!!!"
  ].join( '\n' );

  var pluginIdentifier = /(Transition Effects) : Version - (\d+\.\d+\.\d+)/;

  for ( var i = 0, l = $plugins.length; i < l; i++ ) {
    if ( $plugins[i].description.match( pluginIdentifier ) ) {
      $.alias = {};
      $.name = RegExp.$1;
      $.version = RegExp.$2;
      $.params = Parse( $plugins[i].parameters );
      $.author = "Chaucer";
      break;
    };
  };

  if ( !$.name || !$.version ) throw new Error( errMessage );

//============================================================================


//=============================================================================
// Custom :
//=============================================================================

  //--------------------------------------------------------------------------
  function Parse( object )
  { // parse all data in an object
  //--------------------------------------------------------------------------

    try {
      object = JSON.parse( object );
    } catch (e) {
      object = object;
    } finally {
      if ( Array.isArray( object ) ) {
        var l = object.length;
        for ( var i = 0; i < l; i++ ) { object[i] = Parse( object[i] ); };
      } else if ( typeof object === 'object' ) {
        for ( var key in object ) { object[key] = Parse( object[key] ); };
      }
    }

     return object;

  };

  //-----------------------------------------------------------------------------
  $.versionLessThan = function ( version )
  { // check if the version is less than the value specified.
  //-----------------------------------------------------------------------------

    var comparison = version.split( '.' );
    var current = $.version.split( '.' );

    if ( current[0] < comparison[0] ) return true;
    if ( current[1] < comparison[1] ) return true;
    if ( current[2] < comparison[2] ) return true;

  };

  //-----------------------------------------------------------------------------
  $.versionGreaterThan = function ( version )
  { // check if the version is greater than or equal to the value specified.
  //-----------------------------------------------------------------------------

    var comparison = version.split( '.' );
    var current = $.version.split( '.' );

    if ( current[0] >= comparison[0] ) return true;
    if ( current[1] >= comparison[1] ) return true;
    if ( current[2] >= comparison[2] ) return true;

  };

  $.vertexShader = `
  attribute vec2 aVertexPosition;
  attribute vec2 aTextureCoord;
  uniform mat3 projectionMatrix;
  uniform mat3 filterMatrix;
  varying vec2 vTextureCoord;
  varying vec2 vFilterCoord;

  void main(void){
    gl_Position = vec4((projectionMatrix * vec3(aVertexPosition, 1)).xy, 0.0, 1.0);
    vFilterCoord = ( filterMatrix * vec3( aTextureCoord, 1.0)  ).xy;
    vTextureCoord = aTextureCoord;
  }
  `;

  $.fragmentShader = `
  precision mediump float;

  varying vec2 vTextureCoord;

  uniform sampler2D uSampler;
  uniform vec2 dimensions;
  uniform vec4 filterArea;
  uniform sampler2D mask;
  uniform float threshold;
  uniform float value;
  uniform vec4 color;

  void main( void )
  {

    vec2 coord = ( vTextureCoord * filterArea.xy ) / dimensions;
    vec4 noise = texture2D( mask, coord );
    float saturation = ( ( 2.0 * value + noise.r ) - 1.0 ) * threshold;

    gl_FragColor = texture2D( uSampler, vTextureCoord );
    float orgAlpha = gl_FragColor.a;
    gl_FragColor *= min( 1.0, saturation );

    if ( gl_FragColor.a > 0.05  ) {
      float power = 1.0 - abs( 0.5 - ( gl_FragColor.a / orgAlpha ) ) * 2.0;
      gl_FragColor += color * power;
    }

  }
  `;

  $.makeBitmap = function() {

    var mask = Chaucer.transitions.params.noiseMask;
    var splitIndex  = mask.lastIndexOf( '/' ) + 1;
    var folder = 'img/' + mask.slice( 0, splitIndex );
    var filename = mask.slice( splitIndex, mask.length );

    $.bitmap = ImageManager.loadBitmap( folder, filename );

  }

  $.uniforms = function( bitmap, color, threshold ) {

    threshold = threshold || $.params.threshold;
    color = color || $.params.color;
    bitmap = bitmap || $.bitmap;

    return {
      mask: { type: 'sampler2D', value: new PIXI.Texture( bitmap.baseTexture ) },
      dimensions: { type: 'v2', value: [Graphics.width, Graphics.height] },
      color: { type: 'v4', value: [color.r, color.g, color.b, color.a] },
      threshold: { type: 'f', value: threshold },
      value: { type: 'f', value: 1.0 },
    };

  };

  $.makeBitmap();

//=============================================================================
// Scene_Map :
//=============================================================================

//-----------------------------------------------------------------------------
  $.alias.SM_p_createDisplayObjects = Scene_Map.prototype.createDisplayObjects;
//-----------------------------------------------------------------------------
  Scene_Map.prototype.createDisplayObjects = function ()
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    $.alias.SM_p_createDisplayObjects.call( this );
    this.createBattleTransition();

  };

//-----------------------------------------------------------------------------
  $.alias.SM_p_startEncounterEffect = Scene_Map.prototype.startEncounterEffect;
//-----------------------------------------------------------------------------
  Scene_Map.prototype.startEncounterEffect = function ()
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    $.alias.SM_p_startEncounterEffect.call( this );
    this._bTransitionFilter.fadeOut();

  };

//-----------------------------------------------------------------------------
  $.alias.SM_p_updateEncounterEffect = Scene_Map.prototype.updateEncounterEffect;
//-----------------------------------------------------------------------------
  Scene_Map.prototype.updateEncounterEffect = function ()
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------
    if ( !$.params.enableBTransition ) {
      $.alias.SM_p_updateEncounterEffect.call( this );

    } else if ( !this._bTransitionFilter.isBusy() ) {
      this._encounterEffectDuration = 0;
      BattleManager.playBattleBgm();

    }

  };

//=============================================================================
// Game_Interpreter :
//=============================================================================

//-----------------------------------------------------------------------------
  $.alias.GI_p_pluginCommand = Game_Interpreter.prototype.pluginCommand;
//-----------------------------------------------------------------------------
  Game_Interpreter.prototype.pluginCommand = function ( command, args)
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    $.alias.GI_p_pluginCommand.call( this, command, args );
    command = command.toLowerCase();
    if ( command === 'gl_transition' ) {
      this.requestTransitionEffect( args );
    }

  };

//-----------------------------------------------------------------------------
  $.alias.GI_p_updateWaitMode = Game_Interpreter.prototype.updateWaitMode;
//-----------------------------------------------------------------------------
  Game_Interpreter.prototype.updateWaitMode = function ()
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    if ( this._waitMode === 'transition' ) {
      waiting = this._transition.isBusy();
      if ( !waiting ) this._waitMode = '';
      return waiting;

    } else {
      return $.alias.GI_p_updateWaitMode.call( this );
    }

  };

//=============================================================================
} )( Chaucer.transitions );
//=============================================================================
