/*:============================================================================

  @author Chaucer

  @plugindesc | Resolution : Version - 3.0.0 | This pluign allows you to specify custom resolutions & screen sizes.

  @help
============================================================================
  Introduction :
============================================================================

  ()()
  (^.^)
  c(")(")

  This pluign allows you to specify custom resolutions, and screen sizes, as
  well as allows you to restrict screen size in windows/mac/linux deployed
  games.

============================================================================
  Requirements :
============================================================================

  ---------------------------------------
  None.
  ---------------------------------------

============================================================================
  Instructions :
============================================================================

  ---------------------------------------
  Plugin Commands :
  ---------------------------------------

  ---------------------------------------
  Script calls :
  ---------------------------------------

============================================================================
  Terms Of Use :
============================================================================

  This Plugin may be used commercially, or non commercially. This plugin may
  be extended upon, and or shared freely as long as credit is given to it's
  author(s). This plugin may NOT be sold, or plagiarized.

============================================================================
  Version History :
============================================================================

 ● Version : 1.0.0
 ● Date : 01/01/2017
   ★ Release.

 ● Version : 2.0.0
 ● Date : 12/02/2018
   ★ Rewrote plugin and help file.
   ★ Instantly launch with last used resolution, deploeyed game only.
   ★ If just one resolution is defined, options will not show resolution param.
   ★ Users can define unlimited resolutions instead of just 5.
   ★ Plugin now changes resolution, and NOT screen size.
   ★ Prevent stretching is now optional.

 ● Version : 2.1.0
 ● Date : 12/02/2018
   ★ flexible_resolutions parameter added, see help file for more info.
   ★ Full screen on startup option added( credits to ally @rwm ).
   ✩ Fixed bug which prevented window from using newly defined resolutions.

 ● Version : 2.4.0
 ● Date : 12/02/2018
   ★ Plugin now instantly resizes screen on launch for 1.6.0, even undeployed!
   ✩ Fixed crash which occurs randomly when game launches in full screen.
   ✩ Can now exit full screen when the game was launched in full screen.
   ✩ Fixed bug when changing resolutions while in full screen.
   ✩ Fixed resizing issue when changing to full screen.

 ● Version : 2.5.0
 ● Date : 04/03/2018
   ★ Added prevent full screen toggle button.
   ★ Disabled Maximize button if prevent_stretching is enabled.
   ✩ Game now keeps resolution after minimize( credits to Plueschkatze @rmw ).

 ● Version : 3.0.0
 ● Date : 02/05/2020
   ★ Refactored the plugins code.
   ★ Added fullscreen option in options menu.
   ★ Added ability to disable full screen by F4.
   ★ Dropped support for RPG Maker Versions below 1.6.0.


============================================================================
  Contact Me :
============================================================================

  If you have questions, about this plugin, or commissioning me, or have
  a bug to report, please feel free to contact me by any of the below
  methods.

  rmw : https://forums.rpgmakerweb.com/index.php?members/chaucer.44456
  discord : chaucer#7538
  skypeId : chaucer1991
  gmail : chaucer91

============================================================================
  Support Me :
============================================================================

   If you like the content I create, and want to contribute to help me
  making more plugins on a regular basis, you can donate, or pledge through
  any of the links listed below!

  ko-fi : https://ko-fi.com/chaucer91
  paypal.me : https://paypal.me/chaucer91
  patreon : https://www.patreon.com/chaucer91

============================================================================
  Special Thanks :
============================================================================

  Patrons :

  ★ Benjamin Humphrey

============================================================================

  @param enableMenuFullscreen
  @text Enable Option Fullscreen
  @desc Allow toggling fullscreen from menu.
  @default true
  @type boolean

  @param disableFullScreenButton
  @text Disable Full Screen button
  @desc if enabled the full screen button will be disabled.
  @default false
  @type boolean
  @parent enableMenuFullscreen

  @param enableMenuResolution
  @text Enable Option Resolution
  @desc Show resolution choices in options menu.
  @default true
  @type boolean

  @param resolutions
  @text Resolution Choices
  @desc Set custom resolution sizes here.
  @default ["544x416","640x480","816x624"]
  @type text[]
  @parent enableMenuResolution

  @param preventStretching
  @text Prevent Screen Stretching
  @desc Prevent the player from resizing the screen.
  @default true
  @type boolean

  @param rememberFullscreen
  @text Remember Fullscreen
  @desc If enabled, fullscreen value will be remembered and restored on games next launch.
  @default true
  @type boolean


*/

//=============================================================================
var Imported = Imported || {};
Imported['CHAU RESOLUTION'] = true;
//=============================================================================
var Chaucer = Chaucer || {};
Chaucer.resolution = {};
//=============================================================================

//=============================================================================
// DataManager :
//=============================================================================

//-----------------------------------------------------------------------------
DataManager.getPackagePath = function ()
{ // return the directory for the package.json file.
//-----------------------------------------------------------------------------

  const file = '/package.json';
  const { path } = Chaucer.resolution;
  const filePath = path.dirname( process.mainModule.filename );

  return path ? filePath.replace( '/www', '' ) + file : '';

};

//-----------------------------------------------------------------------------
DataManager.getPackageJson = function ()
{ // get the package.json.
//-----------------------------------------------------------------------------

  const { fs, path } = Chaucer.resolution;

  if ( fs && path ) {

    var filePath = this.getPackagePath();
    return JSON.parse( fs.readFileSync( filePath, 'utf8' ) );

  }

};

//-----------------------------------------------------------------------------
DataManager.saveJsonResolution = function ()
{ // modify the json file and update the resolution.
//-----------------------------------------------------------------------------

  const { fs, path } = Chaucer.resolution;
  if ( fs && path ) { // only proceed if executable file :

    var filePath = this.getPackagePath();
    var json = JSON.parse( fs.readFileSync( filePath, 'utf8' ) );

    if ( json ) { // if json exists, modify and save :

      json.window.width = Graphics.width;
      json.window.height = Graphics.height;
      fs.writeFileSync( filePath, JSON.stringify( json ) );

    }

  }

};

//=============================================================================
// ConfigManager :
//=============================================================================

//-----------------------------------------------------------------------------
ConfigManager.setResolution = function ( resolution )
{ // Set the resolution.
//-----------------------------------------------------------------------------

  this.resolution = resolution;

  if ( this.resolution ) {

    var array = resolution.split( 'x' ).map( Number );
    Chaucer.resolution._forceResolution( array[0], array[1] );

  }

};

//-----------------------------------------------------------------------------
ConfigManager.setFullscreen = function ( value )
{ // set full screen value.
//-----------------------------------------------------------------------------

  this.fullscreen = value;
  if ( Chaucer.resolution.params.rememberFullscreen ) {
    if ( this.fullscreen ) Graphics._requestFullScreen();

  }

};

//=============================================================================
// Window_Options :
//=============================================================================

//-----------------------------------------------------------------------------
Window_Options.prototype.addScreenOptions = function ()
{ // add screen options to the game options menu.
//-----------------------------------------------------------------------------
  var params = Chaucer.resolution.params;
  // enable resolution in options :
  if ( params.enableMenuResolution )
    this.addCommand( 'Resolution Size', 'resolution' );

  // enable fullscreen in options :
  if ( params.enableMenuFullscreen )
    this.addCommand( 'Fullscreen', 'fullscreen' );

};

//-----------------------------------------------------------------------------
Window_Options.prototype.isResolutionSymbol = function ( symbol )
{ // return if this is resolution option.
//-----------------------------------------------------------------------------

  return symbol === 'resolution';

};

//-----------------------------------------------------------------------------
Window_Options.prototype.isFullscreenSymbol = function ( symbol )
{ // return if this is the fullscreen symbol.
//-----------------------------------------------------------------------------
  return symbol === 'fullscreen';
};

//-----------------------------------------------------------------------------
Window_Options.prototype.resolutionStatusText = function ( value )
{ // return the status text for resolution command.
//-----------------------------------------------------------------------------

  return value || ( Graphics.width + 'x' + Graphics.height );

};

//-----------------------------------------------------------------------------
Window_Options.prototype.changeResolution = function ( symbol, value, i )
{ // change the resolution based on the value provided.
//-----------------------------------------------------------------------------

  var resolutions = Chaucer.resolution.validResolutions();
  var index = ( resolutions.indexOf( value ) + i ).mod( resolutions.length );
  var array = resolutions[index].split( 'x' ).map( Number );

  this.changeValue( symbol, resolutions[index] );
  Chaucer.resolution._forceResolution( array[0], array[1] );

  // refresh scene :
  $gameTemp._lastIndex = this.index();
  SceneManager.goto( SceneManager._scene.constructor );

};

//-----------------------------------------------------------------------------
Window_Options.prototype.changeFullScreen = function ( symbol, value )
{ // toggle fullscreen value.
//-----------------------------------------------------------------------------

  this.changeValue( symbol, value );
  ConfigManager.save();

  if ( value ) {
    Graphics._requestFullScreen();

  } else {
    Graphics._cancelFullScreen();

  }

};

( function ( $ ) { // CONFIG:

  $ = $ || {};

//Create plugin information.
//============================================================================

  $.errMessage = [
    "Resolution description has been altered! Either revert",
    "the description of the plugin back to normal, or modify the variable",
    "named pluginIdentifier inside the plugin to prevent further errors!!!"
  ].join( '\n' );

  $.errCompatability = `
  This plugin only supports RPG Maker MV Version 1.6.0+!
  If you wish to use this plugin please consider upgrading game to a
  version that is supported.
  `
  var pluginIdentifier = /(Resolution) : Version - (\d+\.\d+\.\d+)/;
   var length = $plugins.length;
  for ( var i = 0; i < length; i++ ) {
    if ( $plugins[i].description.match( pluginIdentifier ) ) {
      $.alias = {};
      $.name = RegExp.$1;
      $.version = RegExp.$2;
      $.params = Parse( $plugins[i].parameters );
      $.author = "Chaucer";
      break;
    };
  };

  if ( !$.name || !$.version ) throw new Error( $.errMessage );

//============================================================================


//=============================================================================
// Custom :
//=============================================================================

//--------------------------------------------------------------------------
// Local Variables :
//--------------------------------------------------------------------------
  $.gameWindow = Utils.isNwjs() ? require( "nw.gui" ).Window.get() : null;
  $.path = Utils.isNwjs() ? require( 'path' ) : null;
  $.fs = Utils.isNwjs() ? require( 'fs' ) : null;
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
  function Parse( data )
  { // parse data.
//--------------------------------------------------------------------------

    try {

      data = JSON.parse( data ); // try to turn data to object :

    } catch ( error ) {

      data = data; // if failed, data remains a string :

    } finally {

      if ( typeof data === 'object' ) { // if data is an object :

        var keys = Object.keys( data ); // get all keys for object :
        for (var i = 0; i < keys.length; i++) {
          data[keys[i]] = Parse( data[keys[i]] );
        }

      }

    }

    return data;

  };

//-----------------------------------------------------------------------------
  $.compareVersion = function ( current, target )
  { // compare the current version with the target version.
//-----------------------------------------------------------------------------

    var value = 0;

    // prepare version values provided :
    target = target.split( '.' );
    current = current.split( '.' );

    // compare version with target :
    value = Math.sign( current[0] - target[0] );
    if ( value === 0 ) value = Math.sign( current[1] - target[1] );
    if ( value === 0 ) value = Math.sign( current[2] - target[2] );

    return value;

    // -1 ( current < target );
    //  0 ( current == target )
    //  1  ( current > target )

  };

//-----------------------------------------------------------------------------
  $.disableStretch = function()
  { // prevent the screen from stretching.
//-----------------------------------------------------------------------------

    // only disable stretch if is nwjs and prevent stretch turned on.
    if ( $.gameWindow && $.params.preventStretching ) {

      $.enableStretch();

      $.gameWindow.setMinimumSize( Graphics.width, Graphics.height );
      $.gameWindow.setMaximumSize( Graphics.width, Graphics.height );

    }

  };

//-----------------------------------------------------------------------------
  $.enableStretch = function()
  { // allow the screen to stretch.
//-----------------------------------------------------------------------------

    // only enable stretch if is nwjs and prevent stretch turned on.
    if ( $.gameWindow && $.params.preventStretching ) {

      $.gameWindow.setMinimumSize( null, null );
      $.gameWindow.setMaximumSize( null, null );

    }

  };

//-----------------------------------------------------------------------------
  $._forceResolution = function ( width, height )
  { // force the resolution provided.
//-----------------------------------------------------------------------------

    // renderer resize :
    Graphics.width = width;
    Graphics.height = height;
    Graphics.boxWidth = width;
    Graphics.boxHeight = height;

    // window resize( if possible ) :
    $._forceScreenSize();

  };

//-----------------------------------------------------------------------------
  $._forceScreenSize = function ()
  { // resize the game screen.
//-----------------------------------------------------------------------------

    // if nwjs and is not fullscreen :
    if ( $.gameWindow && !$.gameWindow.isFullscreen ) {

      // enable stretch :
      this.enableStretch();

      // disable stretch :
      this.disableStretch();

      // add resize listner :
      $.addResizeHandler();

    }

  };

//-----------------------------------------------------------------------------
  $.validResolutions = function ()
  { // return all resolutions that fit on the current screen size.
//-----------------------------------------------------------------------------

    var valid = [];
    var maxWidth = screen.availWidth;
    var maxHeight = screen.availHeight;
    var resolutions = this.params.resolutions;

    for ( var i = 0, l = resolutions.length; i < l; i++ ) {

      var array = resolutions[i].split( 'x' );
      if ( array[0] <= maxWidth && array[1] <= maxHeight ) {
        valid.push( resolutions[i] );
      }

    }

    return valid;

  };

//-----------------------------------------------------------------------------
$._onResize = function ()
{ // called when the screen resizes.
//-----------------------------------------------------------------------------

  // since resize happens many times, set a time out and clear resize event :
  const timeOutId = setTimeout( function() {

    // refresh( if necessary ) and center the window :
    $._refreshWindow();
    $._centerScreen();

    // clear timeout :
    clearTimeout( timeOutId );

  }, 60 );
  $.removeResizeListener();

};

//-----------------------------------------------------------------------------
$._refreshWindow = function ()
{ // refresh the window if it doesn't resize properly!
//-----------------------------------------------------------------------------

  var resizeWidth = $.gameWindow.width != Graphics.width;
  var resizeHeight = $.gameWindow.height != Graphics.height;

  // refresh the window when resize doesn't happen( rare occurance ) :
  if ( resizeWidth || resizeHeight ) {
    $.gameWindow.hide();
    $.gameWindow.show();
  }

};

//-----------------------------------------------------------------------------
$._centerScreen = function ()
{ // move the window to the center of the computers screen.
//-----------------------------------------------------------------------------

  // get new position for window :
  var w = screen.availWidth / 2 - Graphics.boxWidth / 2;
  var h = screen.availHeight / 2 - Graphics.boxHeight / 2;

  // resize & ensure y doesn't go below 1( or weird stuff can happen ):
  $.gameWindow.moveTo( w, Math.max( h, 1 ) );

};

//-----------------------------------------------------------------------------
$.addResizeHandler = function ()
{ // add a handler for on resize event.
//-----------------------------------------------------------------------------

  $.gameWindow.on( 'resize', $._onResize );

};

//-----------------------------------------------------------------------------
$.removeResizeListener = function ()
{ // remove the resize listener for this event.
//-----------------------------------------------------------------------------

  $.gameWindow.removeListener( 'resize', $._onResize );

};

//=============================================================================
// ERROR CATCH FOR MV VERSION :
//=============================================================================

  if ( $.compareVersion( Utils.RPGMAKER_VERSION, '1.6.0' ) < 0 )
  { // this is an older version of rpg maker!

    throw new Error( $.errCompatability );

  }

//=============================================================================
// Graphics :
//=============================================================================

//-----------------------------------------------------------------------------
  $.alias.G_initialize = Graphics.initialize;
//-----------------------------------------------------------------------------
  Graphics.initialize = function ( width, height, type )
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    //  get width and height from our package.json file :
    const package = DataManager.getPackageJson();
    height = package.window.height || height;
    width = package.window.width || width;

    $.alias.G_initialize.call( this, width, height, type );

  };

//-----------------------------------------------------------------------------
  $.alias.G_onKeyDown = Graphics._onKeyDown;
//-----------------------------------------------------------------------------
  Graphics._onKeyDown = function ( event )
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------



    if ( !event.ctrlKey && !event.altKey && event.keyCode === 115 ) {

      if ( !$.params.disableFullScreenButton ) {

        ConfigManager.fullscreen = !ConfigManager.fullscreen;
        $.alias.G_onKeyDown.call( this, event );
        ConfigManager.save();

      }

    } else {

      $.alias.G_onKeyDown.call( this, event );

    }


  };

//-----------------------------------------------------------------------------
  $.alias.G_requestFullscreen = Graphics._requestFullScreen;
//-----------------------------------------------------------------------------
  Graphics._requestFullScreen = function ()
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    // enable stretch on enter full screen.
    $.enableStretch();
    $.alias.G_requestFullscreen.call( this );
    $.removeResizeListener();

  };

//-----------------------------------------------------------------------------
  $.alias.G_cancelFullscreen = Graphics._cancelFullScreen;
//-----------------------------------------------------------------------------
  Graphics._cancelFullScreen = function ()
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    $.alias.G_cancelFullscreen.call( this );

    // add resize handler :
    $.addResizeHandler();

    // disable stretch on exit full screen.
    $.disableStretch();

  };

//=============================================================================
// ConfigManager :
//=============================================================================

//-----------------------------------------------------------------------------
  $.alias.CM_save = ConfigManager.save;
//-----------------------------------------------------------------------------
  ConfigManager.save = function ()
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------
    $.alias.CM_save.call( this );
    DataManager.saveJsonResolution();
  };

//-----------------------------------------------------------------------------
  $.alias.CM_p_makeData = ConfigManager.makeData;
//-----------------------------------------------------------------------------
  ConfigManager.makeData = function ()
  { // Alias of makeData
//-----------------------------------------------------------------------------
    var config = $.alias.CM_p_makeData.call( this );
    config.resolution = this.resolution;
    config.fullscreen = this.fullscreen;
    return config;

  };

//-----------------------------------------------------------------------------
  $.alias.CM_applyData = ConfigManager.applyData;
//-----------------------------------------------------------------------------
  ConfigManager.applyData = function ( config )
  { // Alias of applyData
//-----------------------------------------------------------------------------

    $.alias.CM_applyData.call( this, config );
    this.setResolution( config['resolution'] );
    this.setFullscreen( this.readFlag( config, 'fullscreen' ) );

  };

//=============================================================================
// Scene_Base :
//=============================================================================

//-----------------------------------------------------------------------------
  $.alias.SMB_p_createBackground = Scene_MenuBase.prototype.createBackground;
//-----------------------------------------------------------------------------
  Scene_MenuBase.prototype.createBackground = function ()
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    $.alias.SMB_p_createBackground.call( this );
    var ox = ( Graphics.width - this._backgroundSprite.width ) / 2;
    var oy = ( Graphics.height - this._backgroundSprite.height ) / 2;
    this._backgroundSprite.position.set( ox, oy );

  };

//=============================================================================
// Scene_Options :
//=============================================================================

//-----------------------------------------------------------------------------
  $.alias.SO_p_start = Scene_Options.prototype.start;
//-----------------------------------------------------------------------------
  Scene_Options.prototype.start = function ()
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    $.alias.SO_p_start.call( this );
    if ( $gameTemp._lastIndex ) {
      this._optionsWindow.select( $gameTemp._lastIndex );
      $gameTemp._lastIndex = undefined;
    }

  };

//=============================================================================
// Window_Options :
//=============================================================================

//-----------------------------------------------------------------------------
  $.alias.WO_p_makeCommandList = Window_Options.prototype.makeCommandList;
//-----------------------------------------------------------------------------
  Window_Options.prototype.makeCommandList = function ()
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    $.alias.WO_p_makeCommandList.call( this );
    this.addScreenOptions();

  };

//-----------------------------------------------------------------------------
  $.alias.WO_p_statusText = Window_Options.prototype.statusText;
//-----------------------------------------------------------------------------
  Window_Options.prototype.statusText = function ( index )
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------
    var symbol = this.commandSymbol( index );
    var value = this.getConfigValue( symbol );

    if ( this.isResolutionSymbol( symbol ) ) {
      return this.resolutionStatusText( value );

    } else {
      return $.alias.WO_p_statusText.call( this, index );

    }

  };

//-----------------------------------------------------------------------------
  $.alias.WO_p_processOk = Window_Options.prototype.processOk;
//-----------------------------------------------------------------------------
  Window_Options.prototype.processOk = function ()
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    var index = this.index();
    var symbol = this.commandSymbol(index);
    var value = this.getConfigValue(symbol);

    if ( this.isResolutionSymbol( symbol ) ) { // change resolution size :
      this.changeResolution( symbol, value, 1 );

    } else if ( this.isFullscreenSymbol( symbol ) ) { // toggle fullscreen :
      this.changeFullScreen( symbol, !value );

    } else { // process default cursor right value :

      $.alias.WO_p_processOk.call( this );

    }

  };

//-----------------------------------------------------------------------------
  $.alias.WO_p_cursorRight = Window_Options.prototype.cursorRight;
//-----------------------------------------------------------------------------
  Window_Options.prototype.cursorRight = function ( wrap )
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    var index = this.index();
    var symbol = this.commandSymbol(index);
    var value = this.getConfigValue(symbol);

    if ( this.isResolutionSymbol( symbol ) ) { // change resolution size :
      this.changeResolution( symbol, value, 1 );

    } else if ( this.isFullscreenSymbol( symbol ) ) { // toggle fullscreen :
      this.changeFullScreen( symbol, !value );

    } else { // process default cursor right value :
      $.alias.WO_p_cursorRight.call( this, wrap );

    }

  };

//-----------------------------------------------------------------------------
  $.alias.WO_p_cursorLeft = Window_Options.prototype.cursorLeft;
//-----------------------------------------------------------------------------
  Window_Options.prototype.cursorLeft = function ( wrap )
  { // Alias of commandEnabled
//-----------------------------------------------------------------------------

    var index = this.index();
    var symbol = this.commandSymbol(index);
    var value = this.getConfigValue(symbol);

    if ( this.isResolutionSymbol( symbol ) ) { // change resolution size :
      this.changeResolution( symbol, value, -1 );

    } else if ( this.isFullscreenSymbol( symbol ) ) { // toggle fullscreen :
      this.changeFullScreen( symbol, !value );

    } else { // process default cursor right value :
      $.alias.WO_p_cursorLeft.call( this, wrap );

    }

  };


//=============================================================================
} ) ( Chaucer.resolution );
//=============================================================================
